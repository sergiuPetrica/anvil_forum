# Command which creates a topic along with its first post.
class CreateTopic
  prepend SimpleCommand

  def initialize(topic_args, post_args)
    @topic_args = topic_args
    @post_args = post_args
  end

  def call
    post = Post.new(@post_args)
    post.build_topic(@topic_args)
    errors.add(:save, 'create_topic_post.failure') unless post.save
    result = post.as_json
    result['topic'] = post.topic.as_json
    result
  end
end
