# Commands which generates breadcrumbs for forums and topics.
class GenerateBreadcrumbs
  prepend SimpleCommand

  # TODO: refactor record_type into constants
  def initialize(record_type, record_id)
    @record_type = record_type
    @record_id = record_id
  end

  # TODO: split this into multiple methods
  def call
    breadcrumbs = []
    if @record_type == 'topic'
      breadcrumbs.push(Topic.select(:id, :title, :forum_id)
                         .where('id = ?', @record_id)
                         .first
                         .as_json
                         .merge(type: 'topic'))
      breadcrumbs += get_forum_ancestors(breadcrumbs.first['forum_id'])
    elsif @record_type == 'forum'
      breadcrumbs = get_forum_ancestors(@record_id)
    end

    breadcrumbs.reverse
  end

  private

  def get_forum_ancestors(forum_id)
    ancestors = []
    ancestors.push(Forum.select(:id, :title, :parent_id)
                     .where('id = ?', forum_id)
                     .first
                     .as_json
                     .merge(type: 'forum'))
    until ancestors.last['parent_id'].nil?
      ancestors.push(Forum.select(:id, :title, :parent_id)
                       .where('id = ?', ancestors.last['parent_id'])
                       .first
                       .as_json
                       .merge(type: 'forum'))
    end

    ancestors
  end
end
