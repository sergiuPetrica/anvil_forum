# Command which fetches a forum and its children with their relevant statistics.
class GetForum
  prepend SimpleCommand

  def initialize(forum_id, current_user)
    @forum_id = forum_id
    @current_user = current_user
  end

  def call
    if @forum_id == '0'
      root_forums = Forum
                    .where('parent_id IS NULL AND required_privileges <= ?',
                           @current_user.privileges)
      forum_hash = {}
      forum_hash['forums'] = root_forums.as_json(include: { category: {} })
      forum_hash['root_forum'] = 1
      forum_hash['breadcrumbs'] = nil

      root_forums.each_with_index do |forum, index|
        forum_hash['forums'][index]['statistics'] =
          Statistics.generate_forum_statistics(forum)
      end
    else
      forum = Forum.find_by_id(@forum_id)
      unless forum.required_privileges <= @current_user.privileges
        errors.add(:privileges, 'get_forum.privileges.failure')
        return
      end

      forum_hash = forum.as_json(include: {
                                   forums: {
                                     include: {
                                       category: {}
                                     }
                                   },
                                   topics: {
                                     include: {
                                       user: {
                                         except:
                                           :password_digest
                                       },
                                       posts: {}
                                     }
                                   }
                                 })

      forum.forums.each_with_index do |subforum, index|
        forum_hash['forums'][index]['statistics'] =
          Statistics.generate_forum_statistics(subforum)
      end
      forum.topics.each_with_index do |topic, index|
        forum_hash['topics'][index]['statistics'] =
          Statistics.generate_topic_statistics(topic)
      end
      forum_hash['root_forum'] = 0
      forum_hash['breadcrumbs'] = GenerateBreadcrumbs
                                  .call('forum', forum_hash['id'])
                                  .result
    end

    forum_hash
  end
end
