# Command which gets a topic with its children and increments the view count.
class GetTopic
  prepend SimpleCommand

  def initialize(topic_id, current_page = nil)
    current_page = 1 if current_page.nil?
    @topic_id = topic_id
    @current_page = current_page
  end

  def call
    # TODO: error handling for below
    topic = Topic.find_by_id(@topic_id)
    posts = Post.where('topic_id = ?', @topic_id)
                .where('deleted IS NULL OR deleted = 0')
                .page(@current_page)

    topic_hash = topic.as_json(include: { user: {} })
    topic_hash[:posts] = posts.as_json(
      include: {
        user: {
          except: :password_digest
        }
      }
    )
    topic_hash[:number_of_pages] = posts.total_pages
    topic_hash[:current_page_number] = posts.current_page
    # TODO: error handling for below
    topic_hash[:breadcrumbs] = GenerateBreadcrumbs.call('topic', topic['id'])
                                                  .result

    # TODO: implement a more sophisticated way of incrementing views
    # Idea: instead of incrementing every time the first page is viewed
    # we should take the URL the user is coming from into account?
    # Right now it increments no matter where the user is coming from.
    topic.increment!(:number_of_views) if @current_page == 1

    topic_hash
  end
end
