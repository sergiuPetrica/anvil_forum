# Command which fetches a user and its relevant statistics.
class GetUser
  prepend SimpleCommand

  def initialize(user_id)
    @user_id = user_id
  end

  def call
    user = User.find_by_id(@user_id)
    if user.nil?
      errors.add(:get_user, 'User not found')
      return
    end
    user_hash = user.as_json(except: %i[password_digest email])
    user_hash['statistics'] = Statistics.generate_user_statistics(user)

    user_hash
  end
end
