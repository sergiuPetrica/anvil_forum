# Command for registering a user into the system.
class RegisterUser
  prepend SimpleCommand

  def initialize(username, email, password)
    @username = username
    @email = email
    @password = password
  end

  def call
    new_user = User.create(
      username: @username,
      email: @email,
      password: @password
    )
    errors.add(:user_registration, new_user.errors.full_messages) unless
      new_user.errors.messages.empty?
  end
end
