# Command which searches relevant records based on keywords.
class Search
  prepend SimpleCommand
  RECORD_TYPE_TO_SEARCH = { users: 0, forums: 1, topics: 2, posts: 3 }.freeze

  # TODO: implement search result relevancy
  # Also make search smarter, pretty sure it'll get a ton of irrelevant results.
  def initialize(search_query, record_types = nil)
    # Generate regex from search query for interrogating the db.
    @search_query = search_query.split(' ').join('|')
    @record_types = record_types
    @result = {}
  end

  def call
    if @record_types.nil?
      search_all
    elsif @record_types.include?(RECORD_TYPE_TO_SEARCH[:users])
      search_users
    elsif @record_types.include?(RECORD_TYPE_TO_SEARCH[:forums])
      search_forums
    elsif @record_types.include?(RECORD_TYPE_TO_SEARCH[:topics])
      search_topics
    elsif @record_types.include?(RECORD_TYPE_TO_SEARCH[:posts])
      search_posts
    else
      errors.add(:search, 'Invalid search record')
    end

    @result
  end

  private

  def search_all
    search_users
    search_forums
    search_topics
    search_posts
  end

  def search_users
    @result[:users] = User.where('username REGEXP ?', @search_query)
                          .as_json(except: :password_digest)
  end

  def search_forums
    @result[:forums] = []
    matching_forums = Forum.where('title REGEXP ? OR subtitle REGEXP ?',
                                  @search_query,
                                  @search_query)
    matching_forums.each do |forum|
      forum_processed = forum.as_json(include: { category: {} })
      forum_processed[:statistics] = Statistics.generate_forum_statistics(forum)
      @result[:forums].push(forum_processed)
    end
  end

  def search_topics
    @result[:topics] = []
    matching_topics = Topic.where('title REGEXP ?', @search_query)
    matching_topics.each do |topic|
      topic_processed = topic.as_json(include: { user: {} })
      topic_processed[:statistics] = Statistics.generate_topic_statistics(topic)
      @result[:topics].push(topic_processed)
    end
  end

  def search_posts
    result[:posts] = Post.where('message REGEXP ?', @search_query)
                         .as_json(include: {
                                    user: { except: %i[password_digest email] },
                                    topic: {}
                                  })
  end
end
