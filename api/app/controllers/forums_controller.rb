# Controller which handles forum-related actions.
class ForumsController < ApplicationController
  before_action :set_forum, only: %i[show update destroy]

  # GET /forums
  def index
    command = GetForum.call('0', current_user)
    render json: command.result
  end

  # GET /forums/1
  def show
    command = GetForum.call(params[:id], current_user)

    if command.success?
      render json: command.result
    else
      render status: 404
    end
  end

  # POST /forums
  def create
    @forum = Forum.new(forum_params)

    if @forum.save
      render json: @forum, status: :created, location: @forum
    else
      render json: @forum.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /forums/1
  def update
    if @forum.update(forum_params)
      render json: @forum
    else
      render json: @forum.errors, status: :unprocessable_entity
    end
  end

  # DELETE /forums/1
  def destroy
    if @forum.update(deleted: 1)
      render status: :no_content
    else
      render status: :unprocessable_entity
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_forum
    @forum = Forum.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def forum_params
    params.require(:forum).permit(:title, :subtitle, :required_privileges, :parent_id)
  end
end
