# Controller which handles user registration.
class RegistrationController < ApplicationController
  skip_before_action :authenticate_request

  # POST /register
  def register
    command = RegisterUser.call(params[:username],
                                params[:email],
                                params[:password])

    if command.success?
      render json: {}, status: 200
    else
      render json: { error: command.errors }, status: :unprocessable_entity
    end
  end
end
