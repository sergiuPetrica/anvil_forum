# Controller which handles search-related actions.
class SearchController < ApplicationController
  # GET /search
  def index
    command = Search.call(params[:search_query])

    if command.success?
      render json: command.result
    else
      render json: {}, status: 404
    end
  end

  private

  # Only allow a trusted parameter "white list" through.
  def search_params
    params.require(:search).permit(:search_query)
  end
end
