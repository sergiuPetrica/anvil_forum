# Controller which handles forum-related actions.
class TopicsController < ApplicationController
  before_action :set_topic, only: %i[show update destroy]

  # GET /topics
  def index
    @topics = Topic.all

    render json: @topics
  end

  # GET /topics/1
  def show
    command = GetTopic.call(params[:id], params[:page_number])

    if command.success?
      render json: command.result
    else
      render response: 404
    end
  end

  # POST /topics
  def create
    command = CreateTopic.call({
                                 title: topic_params[:title],
                                 forum_id: topic_params[:forum_id],
                                 user_id: current_user[:id]
                               },
                               {
                                 message: params[:content],
                                 user_id: current_user[:id]
                               })

    if command.success?
      render json: command.result, status: :created
    else
      render json: command.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /topics/1
  def update
    if @topic.update(topic_params)
      render json: @topic
    else
      render json: @topic.errors, status: :unprocessable_entity
    end
  end

  # DELETE /topics/1
  def destroy
    if @topic.update(deleted: 1)
      render status: :no_content
    else
      render status: :unprocessable_entity
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_topic
    @topic = Topic.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def topic_params
    params.require(:topic).permit(:title, :forum_id, :user_id)
  end
end
