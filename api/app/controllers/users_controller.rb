# Controller which handles user-related actions.
class UsersController < ApplicationController
  before_action :set_user, only: %i[show update destroy]

  # GET /users
  def index
    @users = User.all
                 .order(privileges: :desc, username: :asc)
                 .as_json(except: %i[password_digest email])

    render json: @users
  end

  # GET /users/1
  def show
    command = GetUser.call(params[:id])

    if command.success?
      render json: command.result
    else
      render json: {}, status: :not_found
    end
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    if @user.update(deleted: 1,
                    password: params[:password])
      render status: :no_content
    else
      render status: :unprocessable_entity
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def user_params
    params.require(:user).permit(
      :username,
      :email,
      :password,
      :avatar_path,
      :signature,
      :active,
      :privileges,
      :settings
    )
  end
end
