class Forum < ApplicationRecord
  belongs_to :forum, class_name: :Forum, foreign_key: :parent_id, optional: true
  belongs_to :category, class_name: :Category, foreign_key: :category_id, optional: true
  has_many :forums, class_name: :Forum, foreign_key: :parent_id
  has_many :topics, class_name: :Topic, foreign_key: :forum_id
  validates :title, presence: true

  def descendents
    children = Forum.where(parent_id: id)
    children.flat_map do |child_forum|
      child_forum.descendents << child_forum
    end
  end
end
