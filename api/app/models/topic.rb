class Topic < ApplicationRecord
  belongs_to :forum, class_name: :Forum, foreign_key: :forum_id
  belongs_to :user, class_name: :User, foreign_key: :user_id
  has_many :posts, class_name: :Post, foreign_key: :topic_id
  validates :title, presence: true
end
