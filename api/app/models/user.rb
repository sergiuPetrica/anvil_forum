require 'uri'

class User < ApplicationRecord
  has_secure_password
  validates :username, presence: true
  validates :email, presence: true, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :password, presence: true

  PRIVILEGES = { normal: 0, moderator: 50, administrator: 100 }.freeze
  ACTIVITY = { inactive: 0, active: 1 }.freeze
end
