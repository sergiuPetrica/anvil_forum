Rails.application.routes.draw do
  resources :users
  resources :forums
  resources :topics
  get 'topics/:id/page/:page_number' => 'topics#show'
  resources :posts
  resources :categories
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post 'register', to: 'registration#register'
  post 'authenticate', to: 'authentication#authenticate'
  get 'search/:search_query', to: 'search#index'
end
