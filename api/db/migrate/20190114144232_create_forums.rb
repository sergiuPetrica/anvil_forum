class CreateForums < ActiveRecord::Migration[5.2]
  def change
    create_table :forums do |t|
      t.string :title
      t.string :subtitle
      t.integer :required_privileges
      t.bigint :parent_id

      t.timestamps
    end

    add_foreign_key :forums, :forums, column: :parent_id
  end
end
