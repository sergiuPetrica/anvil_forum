class CreateTopics < ActiveRecord::Migration[5.2]
  def change
    create_table :topics do |t|
      t.string :title
      t.bigint :forum_id
      t.bigint :user_id

      t.timestamps
    end

    add_foreign_key :topics, :forums, column: :forum_id
    add_foreign_key :topics, :users, column: :user_id
  end
end
