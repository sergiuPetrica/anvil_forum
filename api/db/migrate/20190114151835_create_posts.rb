class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :message
      t.bigint :topic_id
      t.bigint :user_id

      t.timestamps
    end

    add_foreign_key :posts, :topics, column: :topic_id
    add_foreign_key :posts, :users, column: :user_id
  end
end
