class ForumAddDefaultValues < ActiveRecord::Migration[5.2]
  def change
    change_column :forums, :required_privileges, :integer, default: 0
  end
end
