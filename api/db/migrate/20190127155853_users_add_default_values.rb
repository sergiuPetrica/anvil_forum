class UsersAddDefaultValues < ActiveRecord::Migration[5.2]
  def change
    change_column :users, :active, :integer, default: 0
    change_column :users, :privileges, :integer, default: 0
  end
end
