class ForumsSetRequiredFields < ActiveRecord::Migration[5.2]
  def change
    change_column :forums, :title, :string, null: false
  end
end
