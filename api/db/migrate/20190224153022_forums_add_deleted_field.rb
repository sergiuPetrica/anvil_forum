class ForumsAddDeletedField < ActiveRecord::Migration[5.2]
  def change
    add_column :forums, :deleted, :boolean, after: :parent_id
  end
end
