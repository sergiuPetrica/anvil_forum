class TopicsSetRequiredFields < ActiveRecord::Migration[5.2]
  def change
    change_column :topics, :title, :string, null: false
  end
end
