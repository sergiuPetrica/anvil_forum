class TopicsAddDeletedField < ActiveRecord::Migration[5.2]
  def change
    add_column :topics, :deleted, :boolean, after: :user_id
  end
end
