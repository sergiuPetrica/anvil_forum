class PostsSetRequiredFields < ActiveRecord::Migration[5.2]
  def change
    change_column :posts, :message, :string, null: false
  end
end
