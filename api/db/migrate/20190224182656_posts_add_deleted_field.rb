class PostsAddDeletedField < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :deleted, :boolean, after: :user_id
  end
end
