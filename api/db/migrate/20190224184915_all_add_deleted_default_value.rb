class AllAddDeletedDefaultValue < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :deleted, :boolean, after: :settings, default: 0
    change_column :forums, :deleted, :boolean, default: 0
    change_column :topics, :deleted, :boolean, default: 0
    change_column :posts, :deleted, :boolean, default: 0
  end
end
