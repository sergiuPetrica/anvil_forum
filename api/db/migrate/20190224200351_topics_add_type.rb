class TopicsAddType < ActiveRecord::Migration[5.2]
  def change
    add_column :topics, :topic_type, :int, after: :title, default: 0
  end
end
