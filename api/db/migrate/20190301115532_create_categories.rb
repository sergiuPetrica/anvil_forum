class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :name, null: false
      t.integer :category_type, default: 0
      t.integer :position, default: 0

      t.timestamps
    end
  end
end
