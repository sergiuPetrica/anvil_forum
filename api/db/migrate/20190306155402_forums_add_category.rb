class ForumsAddCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :forums, :category_id, :integer, after: :parent_id
  end
end
