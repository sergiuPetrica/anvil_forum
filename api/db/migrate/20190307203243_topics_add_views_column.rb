class TopicsAddViewsColumn < ActiveRecord::Migration[5.2]
  def change
    add_column :topics, :number_of_views, :integer, default: 0, after: :topic_type
  end
end
