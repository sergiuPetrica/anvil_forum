class UsersAddDetailColumns < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :birthday, :date, after: :password_digest
    add_column :users, :location, :string, after: :birthday
    add_column :users, :occupation, :string, after: :location
    add_column :users, :mood, :string, after: :occupation
  end
end
