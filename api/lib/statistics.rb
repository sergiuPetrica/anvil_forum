# Class with static methods which generate statistics for various record types.
class Statistics
  def self.generate_forum_statistics(forum)
    forum_id_list = forum.descendents.pluck(:id)
    forum_id_list.push(forum[:id])

    topic_id_list = Topic.select('id')
                         .where("forum_id IN (#{forum_id_list.join(',')})")
                         .where('deleted IS NULL OR deleted = 0')
                         .pluck(:id)

    post_id_list = []
    last_post = nil
    unless topic_id_list.nil? || topic_id_list.empty?
      posts = Post.where("topic_id IN (#{topic_id_list.join(',')})")
                  .where('deleted IS NULL OR deleted = 0')
      post_id_list = posts.pluck(:id)
      last_post = posts.order('created_at').last.as_json(include: {
                                                           topic: {},
                                                           user: {}
                                                         })
    end

    { topic_count: topic_id_list.count,
      post_count: post_id_list.count,
      last_post: last_post }
  end

  def self.generate_topic_statistics(topic)
    post_list = Post.where(topic: topic[:id]).where('deleted IS NULL or deleted = 0')
    last_post = post_list.order('created_at').last.as_json(include: {
                                                             user: {}
                                                           })

    # TODO: Make sure we can't get negative number in post count?
    { post_count: post_list.count - 1,
      last_post: last_post }
  end

  def self.generate_user_statistics(user)
    recent_topics = Topic.where(user_id: user[:id])
                         .order(created_at: :desc)
                         .limit(10)
    recent_topics_hash = recent_topics.as_json(include: {
                                                 forum: {},
                                                 user: {}
                                               })
    recent_topics.each_with_index do |topic, index|
      recent_topics_hash[index]['statistics'] = generate_topic_statistics(topic)
    end
    number_of_topics = Topic.where(user_id: user[:id]).count

    recent_posts_hash = Post.where(user_id: user[:id])
                            .order(created_at: :desc)
                            .limit(10)
                            .as_json(include: %i[topic user])
    number_of_posts = Post.where(user_id: user[:id]).count

    { recent_topics: recent_topics_hash,
      number_of_topics: number_of_topics,
      recent_posts: recent_posts_hash,
      number_of_posts: number_of_posts }

  end
end
