# frozen_string_literal: true

module AuthHelper
  def create_and_login_user(type = nil)
    user = if type == User::PRIVILEGES[:moderator]
             build(:moderator)
           elsif type == User::PRIVILEGES[:administrator]
             build(:administrator)
           else
             build(:user)
           end
    user.save
    command = AuthenticateUser.call(user.username, user.password)
    { user_object: user, login_token: command.result }
  end
end
