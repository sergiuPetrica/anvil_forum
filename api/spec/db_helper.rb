# frozen_string_literal: true

module DbHelper
  def nonexistent_id_for(model_class)
    id_range = 700..999
    loop do
      generated_id = rand(id_range)
      break generated_id unless model_class.exists?(id: generated_id)
    end
  end
end
