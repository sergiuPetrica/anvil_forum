FactoryBot.define do
  factory :invalid_category, class: Category do
    name { nil }
    category_type { nil }
    position { nil }
  end
  factory :category do
    name { 'rspec_category' }
    category_type { 0 }
    position { 0 }
  end

  factory :category_secondary, class: Category do
    name { 'rspec_category_secondary' }
    category_type { 1 }
    position { 0 }
  end
end
