FactoryBot.define do
  factory :invalid_forum, class: Forum do
    title { nil }
    subtitle { nil }
    required_privileges { nil }
    parent_id { nil }
    deleted { nil }
  end

  factory :forum do
    title { 'rspec forum' }
    subtitle { 'rspec subtitle' }
    required_privileges { User::PRIVILEGES[:normal] }
    parent_id { nil }
    deleted { false }
  end

  factory :moderator_forum, class: Forum do
    title { 'rspec moderator forum' }
    subtitle { 'rspec moderator subtitle' }
    required_privileges { User::PRIVILEGES[:moderator] }
    parent_id { nil }
    deleted { false }
  end

  factory :administrator_forum, class: Forum do
    title { 'rspec administrator forum' }
    subtitle { 'rspec administrator subtitle' }
    required_privileges { User::PRIVILEGES[:administrator] }
    parent_id { nil }
    deleted { false }
  end
end
