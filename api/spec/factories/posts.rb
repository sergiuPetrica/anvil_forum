FactoryBot.define do
  factory :invalid_post, class: Post do
    message { nil }
    topic_id { nil }
    user_id { nil }
    deleted { nil }
  end

  factory :post do
    message { 'rspec post' }
    topic_id do
      topic = build(:topic)
      topic.save
      topic.id
    end
    user_id do
      user = build(:user)
      user.save
      user.id
    end
    deleted { false }
  end

  factory :post_secondary, class: Post do
    message { 'rspec secondary post' }
    topic_id do
      topic = build(:topic)
      topic.save
      topic.id
    end
    user_id do
      user = build(:moderator)
      user.save
      user.id
    end
    deleted { false }
  end
end
