FactoryBot.define do
  factory :invalid_topic, class: Topic do
    title { nil }
    topic_type { nil }
    forum_id { nil }
    user_id { nil }
    deleted { nil }
  end

  factory :topic do
    title { 'rspec_topic' }
    topic_type { 0 }
    forum_id do
      forum = build(:forum)
      forum.save
      forum.id
    end
    user_id do
      user = build(:user)
      user.save
      user.id
    end
    deleted { false }
  end

  factory :topic_secondary, class: Topic do
    title { 'rspec_topic_secondary' }
    topic_type { 1 }
    forum_id do
      forum = build(:moderator_forum)
      forum.save
      forum.id
    end
    user_id do
      user = build(:moderator)
      user.save
      user.id
    end
    deleted { false }
  end
end
