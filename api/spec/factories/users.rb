FactoryBot.define do
  factory :invalid_user, class: User do
    username { nil }
    email { nil }
    password { nil }
    avatar_path { nil }
    signature { nil }
    active { nil }
    privileges { nil }
    deleted { nil }
  end

  factory :user do
    username { 'rspec_user' }
    email { 'rspec_user@example.com' }
    password { 'rspec_user_password' }
    avatar_path { '/normal/user/avatar.jpg' }
    signature { 'rspec user signature' }
    active { 1 }
    privileges { User::PRIVILEGES[:normal] }
    deleted { false }
  end

  factory :moderator, class: User do
    username { 'rspec_user' }
    email { 'rspec_user@example.com' }
    password { 'rspec_user_password' }
    avatar_path { '/normal/user/avatar.jpg' }
    signature { 'rspec user signature' }
    active { 1 }
    privileges { User::PRIVILEGES[:normal] }
    deleted { false }
  end

  factory :administrator, class: User do
    username { 'rspec_admin' }
    email { 'rspec_admin@example.com'}
    password { 'rspec_admin_password' }
    avatar_path { '/admin/user/avatar.jpg' }
    signature { 'rspec admin signature' }
    active { 1 }
    privileges { User::PRIVILEGES[:administrator] }
    deleted { false }
  end
end
