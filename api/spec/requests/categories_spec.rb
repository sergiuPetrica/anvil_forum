require 'rails_helper'
require 'auth_helper'
require 'db_helper'

RSpec.describe 'Categories', type: :request do
  include AuthHelper
  include DbHelper
  describe 'GET /categories' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        get categories_path
        expect(response).to have_http_status(401)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_category = build(:category)
        new_category.save
        get categories_path,
            params: {},
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        retrieved_category = JSON.parse(response.body).last
        expect(retrieved_category['id']).to eq(new_category.id)
        expect(retrieved_category['name']).to eq(new_category.name)
        expect(retrieved_category['category_type']).to eq(new_category.category_type)
        expect(retrieved_category['position']).to eq(new_category.position)
      end
    end
  end

  describe 'GET /categories/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        get category_path(1)
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if category doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_category_id = nonexistent_id_for(Category)
        expect do
          get category_path(invalid_category_id),
              params: {},
              headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_category = build(:category)
        new_category.save
        get category_path(new_category),
            params: {},
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        retrieved_category = JSON.parse(response.body)
        expect(retrieved_category['id']).to eq(new_category.id)
        expect(retrieved_category['name']).to eq(new_category.name)
        expect(retrieved_category['category_type']).to eq(new_category.category_type)
        expect(retrieved_category['position']).to eq(new_category.position)
      end
    end
  end

  describe 'POST /categories' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        post categories_path, params: { category: attributes_for(:category) }
        expect(response).to have_http_status(401)
      end
      it "returns an 'unprocessable entity' response
            if parameters are invalid" do
        logged_in_user = create_and_login_user
        post categories_path,
             params: { category: attributes_for(:invalid_category) },
             headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(422)
      end
    end
    context "valid request" do
      it "returns a 'created' response if logged in" do
        logged_in_user = create_and_login_user
        category_params = attributes_for(:category)
        post categories_path,
             params: { category: category_params },
             headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(201)
        expect(response.content_type).to eq('application/json')
        retrieved_category = JSON.parse(response.body)
        expect(retrieved_category['name']).to eq(category_params[:name])
        expect(retrieved_category['category_type'])
          .to eq(category_params[:category_type])
        expect(retrieved_category['position']).to eq(category_params[:position])
      end
    end
  end

  describe 'PATCH/PUT /categories/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        put category_path(1), params: { category: attributes_for(:moderator) }
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if category doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_category_id = nonexistent_id_for(Category)
        expect do
          put category_path(invalid_category_id),
              params: { category: attributes_for(:invalid_category) },
              headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
      it "returns an 'unprocessable entity' response
            if parameters are invalid" do
        logged_in_user = create_and_login_user
        new_category = build(:category)
        new_category.save
        put category_path(new_category.id),
            params: { category: attributes_for(:invalid_category) },
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(422)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_category = build(:category)
        new_category.save
        put category_path(new_category.id),
            params: { category: attributes_for(:moderator) },
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        new_category.reload
        retrieved_category = JSON.parse(response.body)
        expect(retrieved_category['id']).to eq(new_category.id)
        expect(retrieved_category['name']).to eq(new_category.name)
        expect(retrieved_category['category_type'])
          .to eq(new_category.category_type)
        expect(retrieved_category['position']).to eq(new_category.position)
      end
    end
  end

  describe 'DELETE /categories/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        delete category_path(1)
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if category doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_category_id = nonexistent_id_for(Category)
        expect do
          delete category_path(invalid_category_id),
                 params: { category: attributes_for(:category) },
                 headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
    context "valid request" do
      it "returns a 'no content' response if logged in" do
        logged_in_user = create_and_login_user
        new_category = build(:category_secondary)
        new_category.save
        expect do
          delete category_path(new_category.to_param),
                 params: {},
                 headers: { Authorization: logged_in_user[:login_token] }
        end.to change(Category, :count).by(-1)
        expect(response).to have_http_status(204)
      end
    end
  end
end
