require 'rails_helper'
require 'auth_helper'
require 'db_helper'

RSpec.describe 'Forums', type: :request do
  include AuthHelper
  include DbHelper
  describe 'GET /forums' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        get forums_path
        expect(response).to have_http_status(401)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_forum = build(:forum)
        new_forum.save
        get forums_path,
            params: {},
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        retrieved_forum = JSON.parse(response.body)['forums'].last
        expect(retrieved_forum['id']).to eq(new_forum.id)
        expect(retrieved_forum['title']).to eq(new_forum.title)
        expect(retrieved_forum['subtitle']).to eq(new_forum.subtitle)
        expect(retrieved_forum['required_privileges'])
          .to eq(new_forum.required_privileges)
        expect(retrieved_forum['parent_id']).to eq(new_forum.parent_id)
        expect(retrieved_forum['deleted']).to eq(new_forum.deleted)
      end
    end
  end

  describe 'GET /forums/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        get forum_path(1)
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if forum doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_forum_id = nonexistent_id_for(Forum)
        expect do
          get forum_path(invalid_forum_id),
              params: {},
              headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_forum = build(:forum)
        new_forum.save
        get forum_path(new_forum),
            params: {},
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        retrieved_forum = JSON.parse(response.body)
        expect(retrieved_forum['id']).to eq(new_forum.id)
        expect(retrieved_forum['title']).to eq(new_forum.title)
        expect(retrieved_forum['subtitle']).to eq(new_forum.subtitle)
        expect(retrieved_forum['required_privileges'])
          .to eq(new_forum.required_privileges)
        expect(retrieved_forum['parent_id']).to eq(new_forum.parent_id)
        expect(retrieved_forum['deleted']).to eq(new_forum.deleted)
      end
    end
  end

  describe 'POST /forums' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        post forums_path, params: { forum: attributes_for(:forum) }
        expect(response).to have_http_status(401)
      end
      it "returns an 'unprocessable entity' response
            if parameters are invalid" do
        logged_in_user = create_and_login_user
        post forums_path,
             params: { forum: attributes_for(:invalid_forum) },
             headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(422)
      end
    end
    context "valid request" do
      it "returns a 'created' response if logged in" do
        logged_in_user = create_and_login_user
        forum_params = attributes_for(:forum)
        post forums_path,
             params: { forum: forum_params },
             headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(201)
        expect(response.content_type).to eq('application/json')
        retrieved_forum = JSON.parse(response.body)
        expect(retrieved_forum['title']).to eq(forum_params[:title])
        expect(retrieved_forum['subtitle']).to eq(forum_params[:subtitle])
        expect(retrieved_forum['required_privileges'])
          .to eq(forum_params[:required_privileges])
        expect(retrieved_forum['parent_id']).to eq(forum_params[:parent_id])
        expect(retrieved_forum['deleted']).to eq(forum_params[:deleted])
      end
    end
  end

  describe 'PATCH/PUT /forums/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        put forum_path(1), params: { forum: attributes_for(:moderator) }
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if forum doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_forum_id = nonexistent_id_for(Forum)
        expect do
          put forum_path(invalid_forum_id),
              params: { forum: attributes_for(:invalid_forum) },
              headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
      it "returns an 'unprocessable entity' response
            if parameters are invalid" do
        logged_in_user = create_and_login_user
        new_forum = build(:forum)
        new_forum.save
        put forum_path(new_forum.id),
            params: { forum: attributes_for(:invalid_forum) },
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(422)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_forum = build(:forum)
        new_forum.save
        put forum_path(new_forum.id),
            params: { forum: attributes_for(:moderator) },
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        new_forum.reload
        retrieved_forum = JSON.parse(response.body)
        expect(retrieved_forum['id']).to eq(new_forum.id)
        expect(retrieved_forum['title']).to eq(new_forum.title)
        expect(retrieved_forum['subtitle']).to eq(new_forum.subtitle)
        expect(retrieved_forum['required_privileges'])
          .to eq(new_forum.required_privileges)
        expect(retrieved_forum['parent_id']).to eq(new_forum.parent_id)
        expect(retrieved_forum['deleted']).to eq(new_forum.deleted)
      end
    end
  end

  describe 'DELETE /forums/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        delete forum_path(1)
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if forum doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_forum_id = nonexistent_id_for(Forum)
        expect do
          delete forum_path(invalid_forum_id),
                 params: { forum: attributes_for(:forum) },
                 headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
    context "valid request" do
      it "returns a 'no content' response if logged in" do
        logged_in_user = create_and_login_user
        new_forum = build(:moderator_forum)
        new_forum.save
        delete forum_path(new_forum.to_param),
               params: {},
               headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(204)
        new_forum.reload
        expect(new_forum.deleted).to eq(true)
      end
    end
  end
end
