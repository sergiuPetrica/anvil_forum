require 'rails_helper'
require 'auth_helper'
require 'db_helper'

RSpec.describe 'Posts', type: :request do
  include AuthHelper
  include DbHelper
  describe 'GET /posts' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        get posts_path
        expect(response).to have_http_status(401)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_post = build(:post)
        new_post.save
        get posts_path,
            params: {},
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        retrieved_post = JSON.parse(response.body).last
        expect(retrieved_post['id']).to eq(new_post.id)
        expect(retrieved_post['message']).to eq(new_post.message)
        expect(retrieved_post['topic_id']).to eq(new_post.topic_id)
        expect(retrieved_post['user_id']).to eq(new_post.user_id)
        expect(retrieved_post['deleted']).to eq(new_post.deleted)
      end
    end
  end

  describe 'GET /posts/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        get post_path(1)
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if post doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_post_id = nonexistent_id_for(Post)
        expect do
          get post_path(invalid_post_id),
              params: {},
              headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_post = build(:post)
        new_post.save
        get post_path(new_post),
            params: {},
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        retrieved_post = JSON.parse(response.body)
        expect(retrieved_post['id']).to eq(new_post.id)
        expect(retrieved_post['message']).to eq(new_post.message)
        expect(retrieved_post['topic_id']).to eq(new_post.topic_id)
        expect(retrieved_post['user_id']).to eq(new_post.user_id)
        expect(retrieved_post['deleted']).to eq(new_post.deleted)
      end
    end
  end

  describe 'POST /posts' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        post posts_path, params: { post: attributes_for(:post) }
        expect(response).to have_http_status(401)
      end
      it "returns an 'unprocessable entity' response
            if parameters are invalid" do
        logged_in_user = create_and_login_user
        post posts_path,
             params: { post: attributes_for(:invalid_post) },
             headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(422)
      end
    end
    context "valid request" do
      it "returns a 'created' response if logged in" do
        logged_in_user = create_and_login_user
        post_params = attributes_for(:post)
        post_params[:user_id] = logged_in_user[:user_object].id
        post posts_path,
             params: { post: post_params, content: 'rspec post' },
             headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(201)
        expect(response.content_type).to eq('application/json')
        retrieved_post = JSON.parse(response.body)
        expect(retrieved_post['message']).to eq(post_params[:message])
        expect(retrieved_post['topic_id']).to eq(post_params[:topic_id])
        expect(retrieved_post['user_id']).to eq(post_params[:user_id])
        expect(retrieved_post['deleted']).to eq(post_params[:deleted])
      end
    end
  end

  describe 'PATCH/PUT /posts/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        put post_path(1), params: { post: attributes_for(:moderator) }
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if post doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_post_id = nonexistent_id_for(Post)
        expect do
          put post_path(invalid_post_id),
              params: { post: attributes_for(:invalid_post) },
              headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
      it "returns an 'unprocessable entity' response
            if parameters are invalid" do
        logged_in_user = create_and_login_user
        new_post = build(:post)
        new_post.save
        put post_path(new_post.id),
            params: { post: attributes_for(:invalid_post) },
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(422)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_post = build(:post)
        new_post.save
        put post_path(new_post.id),
            params: { post: attributes_for(:moderator) },
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        new_post.reload
        retrieved_post = JSON.parse(response.body)
        expect(retrieved_post['id']).to eq(new_post.id)
        expect(retrieved_post['message']).to eq(new_post.message)
        expect(retrieved_post['topic_id']).to eq(new_post.topic_id)
        expect(retrieved_post['user_id']).to eq(new_post.user_id)
        expect(retrieved_post['deleted']).to eq(new_post.deleted)
      end
    end
  end

  describe 'DELETE /posts/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        delete post_path(1)
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if post doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_post_id = nonexistent_id_for(Post)
        expect do
          delete post_path(invalid_post_id),
                 params: { post: attributes_for(:post) },
                 headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
    context "valid request" do
      it "returns a 'no content' response if logged in" do
        logged_in_user = create_and_login_user
        new_post = build(:post_secondary)
        new_post.save
        delete post_path(new_post.to_param),
               params: {},
               headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(204)
        new_post.reload
        expect(new_post.deleted).to eq(true)
      end
    end
  end
end
