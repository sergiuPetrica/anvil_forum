require 'rails_helper'
require 'auth_helper'
require 'db_helper'

RSpec.describe 'Topics', type: :request do
  include AuthHelper
  include DbHelper
  describe 'GET /topics' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        get topics_path
        expect(response).to have_http_status(401)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_topic = build(:topic)
        new_topic.save
        get topics_path,
            params: {},
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        retrieved_topic = JSON.parse(response.body).last
        expect(retrieved_topic['id']).to eq(new_topic.id)
        expect(retrieved_topic['title']).to eq(new_topic.title)
        expect(retrieved_topic['topic_type']).to eq(new_topic.topic_type)
        expect(retrieved_topic['forum_id']).to eq(new_topic.forum_id)
        expect(retrieved_topic['user_id']).to eq(new_topic.user_id)
        expect(retrieved_topic['deleted']).to eq(new_topic.deleted)
      end
    end
  end

  describe 'GET /topics/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        get topic_path(1)
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if topic doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_topic_id = nonexistent_id_for(Topic)
        expect do
          get topic_path(invalid_topic_id),
              params: {},
              headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_topic = build(:topic)
        new_topic.save
        get topic_path(new_topic),
            params: {},
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        retrieved_topic = JSON.parse(response.body)
        expect(retrieved_topic['id']).to eq(new_topic.id)
        expect(retrieved_topic['title']).to eq(new_topic.title)
        expect(retrieved_topic['topic_type']).to eq(new_topic.topic_type)
        expect(retrieved_topic['forum_id']).to eq(new_topic.forum_id)
        expect(retrieved_topic['user_id']).to eq(new_topic.user_id)
        expect(retrieved_topic['deleted']).to eq(new_topic.deleted)
      end
    end
  end

  describe 'POST /topics' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        post topics_path, params: { topic: attributes_for(:topic) }
        expect(response).to have_http_status(401)
      end
      it "returns an 'unprocessable entity' response
            if parameters are invalid" do
        logged_in_user = create_and_login_user
        post topics_path,
             params: { topic: attributes_for(:invalid_topic) },
             headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(422)
      end
    end
    context "valid request" do
      it "returns a 'created' response if logged in" do
        logged_in_user = create_and_login_user
        topic_params = attributes_for(:topic)
        topic_params[:user_id] = logged_in_user[:user_object].id
        post topics_path,
             params: { topic: topic_params, content: 'rspec post' },
             headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(201)
        expect(response.content_type).to eq('application/json')
        retrieved_topic = JSON.parse(response.body)['topic']
        expect(retrieved_topic['title']).to eq(topic_params[:title])
        expect(retrieved_topic['topic_type']).to eq(topic_params[:topic_type])
        expect(retrieved_topic['forum_id']).to eq(topic_params[:forum_id])
        expect(retrieved_topic['user_id']).to eq(topic_params[:user_id])
        expect(retrieved_topic['deleted']).to eq(topic_params[:deleted])
      end
    end
  end

  describe 'PATCH/PUT /topics/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        put topic_path(1), params: { topic: attributes_for(:moderator) }
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if topic doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_topic_id = nonexistent_id_for(Topic)
        expect do
          put topic_path(invalid_topic_id),
              params: { topic: attributes_for(:invalid_topic) },
              headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
      it "returns an 'unprocessable entity' response
            if parameters are invalid" do
        logged_in_user = create_and_login_user
        new_topic = build(:topic)
        new_topic.save
        put topic_path(new_topic.id),
            params: { topic: attributes_for(:invalid_topic) },
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(422)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_topic = build(:topic)
        new_topic.save
        put topic_path(new_topic.id),
            params: { topic: attributes_for(:moderator) },
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        new_topic.reload
        retrieved_topic = JSON.parse(response.body)
        expect(retrieved_topic['id']).to eq(new_topic.id)
        expect(retrieved_topic['title']).to eq(new_topic.title)
        expect(retrieved_topic['topic_type']).to eq(new_topic.topic_type)
        expect(retrieved_topic['forum_id']).to eq(new_topic.forum_id)
        expect(retrieved_topic['user_id']).to eq(new_topic.user_id)
        expect(retrieved_topic['deleted']).to eq(new_topic.deleted)
      end
    end
  end

  describe 'DELETE /topics/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        delete topic_path(1)
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if topic doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_topic_id = nonexistent_id_for(Topic)
        expect do
          delete topic_path(invalid_topic_id),
                 params: { topic: attributes_for(:topic) },
                 headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
    context "valid request" do
      it "returns a 'no content' response if logged in" do
        logged_in_user = create_and_login_user
        new_topic = build(:topic_secondary)
        new_topic.save
        delete topic_path(new_topic.to_param),
               params: {},
               headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(204)
        new_topic.reload
        expect(new_topic.deleted).to eq(true)
      end
    end
  end
end
