require 'rails_helper'
require 'auth_helper'
require 'db_helper'

RSpec.describe 'Users', type: :request do
  include AuthHelper
  include DbHelper
  describe 'GET /users' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        get users_path
        expect(response).to have_http_status(401)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_user = build(:user)
        new_user.save
        get users_path,
            params: {},
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        retrieved_user = JSON.parse(response.body).last
        expect(retrieved_user['id']).to eq(new_user.id)
        expect(retrieved_user['username']).to eq(new_user.username)
        expect(retrieved_user['email']).to eq(nil)
        expect(retrieved_user['password_digest']).to eq(nil)
        expect(retrieved_user['birthday']).to eq(new_user.birthday)
        expect(retrieved_user['location']).to eq(new_user.location)
        expect(retrieved_user['occupation']).to eq(new_user.occupation)
        expect(retrieved_user['mood']).to eq(new_user.mood)
        expect(retrieved_user['avatar_path']).to eq(new_user.avatar_path)
        expect(retrieved_user['signature']).to eq(new_user.signature)
        expect(retrieved_user['active']).to eq(new_user.active)
        expect(retrieved_user['privileges']).to eq(new_user.privileges)
        expect(retrieved_user['settings']).to eq(new_user.settings)
        expect(retrieved_user['deleted']).to eq(new_user.deleted)
      end
    end
  end

  describe 'GET /users/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        get user_path(1)
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if user doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_user_id = nonexistent_id_for(User)
        expect do
          get user_path(invalid_user_id),
              params: {},
              headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_user = build(:user)
        new_user.save
        get user_path(new_user),
            params: {},
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        retrieved_user = JSON.parse(response.body)
        expect(retrieved_user['id']).to eq(new_user.id)
        expect(retrieved_user['username']).to eq(new_user.username)
        expect(retrieved_user['email']).to eq(nil)
        expect(retrieved_user['password_digest']).to eq(nil)
        expect(retrieved_user['birthday']).to eq(new_user.birthday)
        expect(retrieved_user['location']).to eq(new_user.location)
        expect(retrieved_user['occupation']).to eq(new_user.occupation)
        expect(retrieved_user['mood']).to eq(new_user.mood)
        expect(retrieved_user['avatar_path']).to eq(new_user.avatar_path)
        expect(retrieved_user['signature']).to eq(new_user.signature)
        expect(retrieved_user['active']).to eq(new_user.active)
        expect(retrieved_user['privileges']).to eq(new_user.privileges)
        expect(retrieved_user['settings']).to eq(new_user.settings)
        expect(retrieved_user['deleted']).to eq(new_user.deleted)
      end
    end
  end

  describe 'POST /users' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        post users_path, params: { user: attributes_for(:user) }
        expect(response).to have_http_status(401)
      end
      it "returns an 'unprocessable entity' response
            if parameters are invalid" do
        logged_in_user = create_and_login_user
        post users_path,
             params: { user: attributes_for(:invalid_user) },
             headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(422)
      end
    end
    context "valid request" do
      it "returns a 'created' response if logged in" do
        logged_in_user = create_and_login_user
        user_params = attributes_for(:user)
        post users_path,
             params: { user: user_params },
             headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(201)
        expect(response.content_type).to eq('application/json')
        retrieved_user = JSON.parse(response.body)
        expect(retrieved_user['username']).to eq(user_params[:username])
        expect(retrieved_user['email']).to eq(user_params[:email])
        # TODO: figure out how to test password validity
        expect(retrieved_user['birthday']).to eq(user_params[:birthday])
        expect(retrieved_user['location']).to eq(user_params[:location])
        expect(retrieved_user['occupation']).to eq(user_params[:occupation])
        expect(retrieved_user['mood']).to eq(user_params[:mood])
        expect(retrieved_user['avatar_path']).to eq(user_params[:avatar_path])
        expect(retrieved_user['signature']).to eq(user_params[:signature])
        expect(retrieved_user['active']).to eq(user_params[:active])
        expect(retrieved_user['privileges']).to eq(user_params[:privileges])
        expect(retrieved_user['settings']).to eq(user_params[:settings])
        expect(retrieved_user['deleted']).to eq(user_params[:deleted])
      end
    end
  end

  describe 'PATCH/PUT /users/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        put user_path(1), params: { user: attributes_for(:moderator) }
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if user doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_user_id = nonexistent_id_for(User)
        expect do
          put user_path(invalid_user_id),
              params: { user: attributes_for(:invalid_user) },
              headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
      it "returns an 'unprocessable entity' response
            if parameters are invalid" do
        logged_in_user = create_and_login_user
        new_user = build(:user)
        new_user.save
        put user_path(new_user.id),
            params: { user: attributes_for(:invalid_user) },
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(422)
      end
    end
    context "valid request" do
      it "returns a 'success' response if logged in" do
        logged_in_user = create_and_login_user
        new_user = build(:user)
        new_user.save
        put user_path(new_user.id),
            params: { user: attributes_for(:moderator) },
            headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq('application/json')
        new_user.reload
        retrieved_user = JSON.parse(response.body)
        expect(retrieved_user['id']).to eq(new_user.id)
        expect(retrieved_user['username']).to eq(new_user.username)
        expect(retrieved_user['email']).to eq(new_user.email)
        expect(retrieved_user['password_digest']).to eq(new_user.password_digest)
        expect(retrieved_user['avatar_path']).to eq(new_user.avatar_path)
        expect(retrieved_user['signature']).to eq(new_user.signature)
        expect(retrieved_user['active']).to eq(new_user.active)
        expect(retrieved_user['privileges']).to eq(new_user.privileges)
        expect(retrieved_user['settings']).to eq(new_user.settings)
        expect(retrieved_user['deleted']).to eq(new_user.deleted)
      end
    end
  end

  describe 'DELETE /users/:id' do
    context "invalid request" do
      it "returns an 'unauthorized' response if not logged in" do
        delete user_path(1)
        expect(response).to have_http_status(401)
      end
      it "raises a 'RecordNotFound' error if user doesn't exist" do
        logged_in_user = create_and_login_user
        invalid_user_id = nonexistent_id_for(User)
        expect do
          delete user_path(invalid_user_id),
                 params: { user: attributes_for(:user) },
                 headers: { Authorization: logged_in_user[:login_token] }
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
    context "valid request" do
      it "returns a 'no content' response if logged in" do
        logged_in_user = create_and_login_user
        new_user = build(:moderator)
        new_user.save
        delete user_path(new_user.to_param),
               params: { password: new_user.password },
               headers: { Authorization: logged_in_user[:login_token] }
        expect(response).to have_http_status(204)
        new_user.reload
        expect(new_user.deleted).to eq(true)
      end
    end
  end
end
