import React from 'react';

import './style/app.css'
import AnvilRouter from './router';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faQuoteRight } from '@fortawesome/free-solid-svg-icons';
import { faEdit, faTrashAlt, faFlag } from '@fortawesome/free-regular-svg-icons';
library.add(faEdit, faTrashAlt, faFlag, faQuoteRight);

const App = () => (
    <div className='container-fluid'>
        <AnvilRouter />
    </div>
);

export default App;
