import React from 'react';
import {withRouter} from 'react-router';
import {Link} from "react-router-dom";

import '../style/breadcrumbs.css';

const Breadcrumbs = props => {
    props.items.unshift({id: 0, title: 'Forum index'});

    return props.items.map((currentItem, i) => {
        return (
            <>
                <Link className={(props.items.length - 1) === i ? 'breadcrumb-active' : ''}
                      to={
                          currentItem.type === 'forum'
                              ? currentItem.id === 0 ? '/' : '/forum/' + currentItem.id
                              : '/topic/' + currentItem.id
                      }
                >
                    {currentItem.title}
                </Link>
                <span>
                    {(props.items.length - 1) !== i ? ' ' + String.fromCharCode(8594) + ' ' : ''}
                </span>
            </>
        );
    });
};

export default withRouter(Breadcrumbs);
