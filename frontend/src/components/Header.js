import React from 'react';
import { Link } from 'react-router-dom';

import * as consts from '../constants';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchQuery: null,
            showSearchBox: false,
            error: null,
            success: null
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleSearchClick = this.handleSearchClick.bind(this);
    }

    handleInputChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSearchClick(event) {
        event.preventDefault();
        this.setState({showSearchBox: true});
    }

    handleFormSubmit(event) {
        event.preventDefault();
        this.props.history.push('/search/' + this.state.searchQuery);
        this.setState({showSearchBox: false});
    }

    render() {
        let searchLink = (
            <Link to='/' className="nav-link"
                  onClick={this.handleSearchClick}>
                Search
            </Link>
        );

        if (this.state.showSearchBox === true) {
            searchLink = (
                <form onSubmit={this.handleFormSubmit}>
                    <label>
                        <input className='form-control'
                               type="text"
                               name="searchQuery"
                               onChange={this.handleInputChange} />
                    </label>
                    <br/>
                    <input type="submit" value="Submit" hidden={true} />
                </form>
            );
        }

        return (
            <div className="col-sm-12 navbar-container">
                <nav className="navbar navbar-expand-md align-items-end">
                    <Link to='/' className="navbar-brand">
                        <img className="header-logo"
                             src={consts.FORUM_HEADER_IMAGE_PATH} alt="anvil forum logo"/>
                    </Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarNav" aria-controls="navbarNav"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav nav-fill w-100">
                            <li className="nav-item active">
                                <Link to='/' className="nav-link">
                                    Home
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to='/userlist' className="nav-link">
                                    User List
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to='/' className="nav-link disabled">
                                    User Control Panel
                                </Link>
                            </li>
                            <li className="nav-item">
                                {searchLink}
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        );
    }
}

export default Header;