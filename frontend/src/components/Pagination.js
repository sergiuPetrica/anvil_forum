import React from 'react';
import { withRouter } from 'react-router';
import {Link} from "react-router-dom";

const Pagination = props => {
    let paginationMarkup = '';
    if (props.number_of_pages > 1) {
        paginationMarkup = [];
        let topicUrl = '/topic/' + props.topic_id;
        let pagesUntilFirstPage = parseInt(props.current_page_number) - 1;
        let pagesUntilLastPage = parseInt(props.number_of_pages) - parseInt(props.current_page_number);

        if (pagesUntilFirstPage > 1 ) {
            paginationMarkup.push(
                <span className='topic-first-page'>
                    <Link to={topicUrl}> {'<<'} </Link>
                </span>
            );
        }
        if (pagesUntilFirstPage > 0) {
            let pageUrl = '/page/' + (props.current_page_number - 1);
            paginationMarkup.push(
                <span className='topic-previous-page'>
                    <Link to={topicUrl + pageUrl}> {'<'} </Link>
                </span>
            );
        }
        paginationMarkup.push('Page ');
        paginationMarkup.push(props.current_page_number);
        paginationMarkup.push(' of ');
        paginationMarkup.push(props.number_of_pages);
        if (pagesUntilLastPage > 0) {
            let pageUrl = '/page/' + (props.current_page_number + 1);
            paginationMarkup.push(
                <span className='topic-next-page'>
                    <Link to={topicUrl + pageUrl}> {'>'} </Link>
                </span>
            );
        }
        if (pagesUntilLastPage > 1) {
            let pageUrl = '/page/' + props.number_of_pages;
            paginationMarkup.push(
                <span className='topic-last-page'>
                    <Link to={topicUrl + pageUrl}> {'>>'} </Link>
                </span>
            );
        }
    }

    return paginationMarkup;
};

export default withRouter(Pagination);
