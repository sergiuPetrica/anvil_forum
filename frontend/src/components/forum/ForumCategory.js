import React from 'react';

const ForumCategory = props => {
    if (typeof(props.category) !== 'undefined') {
        return (
            <div className="row category-entry mx-1 py-3">
                <div className="col">
                    <h5>
                        # {props.category.name}
                    </h5>
                </div>
            </div>
        );
    } else return null;
};

export default ForumCategory;
