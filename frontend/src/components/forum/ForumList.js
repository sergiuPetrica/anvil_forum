import React from 'react';

import ForumCategory from './ForumCategory'
import ForumSingle from './ForumSingle';

const NO_CATEGORY_TAG = 'Uncategorized';

const ForumList = props => {
    let header = <h4 className="my-3 forums-header">Forums</h4>;

    if (typeof(props.noCategories) === 'undefined' || props.noCategories !== true) {
        props.forums.sort((a, b) => {
            if (typeof(a.category) === 'undefined') {
                return 1;
            } else if (typeof(b.category) === 'undefined') {
                return -1;
            } else {
                return a.category.position - b.category.position
            }
        });
    }

    let lastCategory = null;
    let forumList = props.forums.map((currentForum, i) => {
        let categoryToInsert = null;
        if (typeof(props.noCategories) === 'undefined' || props.noCategories !== true) {
            // Insert categories where necessary.
            if (typeof(currentForum.category) === 'undefined' &&
                    lastCategory === NO_CATEGORY_TAG) {
                lastCategory = NO_CATEGORY_TAG;
            } else if (typeof(currentForum.category) === 'undefined') {
                lastCategory = NO_CATEGORY_TAG;
                categoryToInsert = <ForumCategory category={{name: NO_CATEGORY_TAG}}
                                                  key={'categ' + i}/>;
            } else if (lastCategory !== currentForum.category.name) {
                lastCategory = currentForum.category.name;
                categoryToInsert = <ForumCategory category={currentForum.category}
                                                  key={'categ' + i}/>;
            }
        }

        let forumToInsert = (
            <ForumSingle forum={currentForum}
                         searchQuery={props.searchQuery}
                         key={'forum' + i} />
        );

        // TODO: find another solution for the span with key
        // Probably not a good idea to have category + next element grouped up
        // under the same span.
        return (
            <span key={i}>
                {categoryToInsert}
                {forumToInsert}
            </span>
        );
    });

    if (forumList.length > 0) {
        return (
            <>
                <div className="row">
                    <div className="col">
                        {header}
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <div className='forum-list'>
                            {forumList}
                        </div>
                    </div>
                </div>
            </>
        );
    } else return null;
};

export default ForumList;
