import React from 'react';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Highlighter from 'react-highlight-words';

const ForumSingle = (props) => {
    let forumStatistics = (
        props.forum.statistics.topic_count + ' threads, ' +
        props.forum.statistics.post_count + ' posts'
    );

    let lastPost = 'no posts';
    if (props.forum.statistics.last_post !== null) {
        lastPost = [];
        lastPost.push('last post by ');
        lastPost.push(
            <Link to={'/user/' + props.forum.statistics.last_post.user.id}
                  className='forum-user'>
                {props.forum.statistics.last_post.user.username}
            </Link>
        );
        lastPost.push(' in ');
        lastPost.push(
            <Link to={'/topic/' + props.forum.statistics.last_post.topic.id}>
                {props.forum.statistics.last_post.topic.title}
            </Link>
        );
        lastPost.push(' ');
        lastPost.push(
            moment(props.forum.statistics.last_post.created_at).fromNow().toString()
        );
    }

    let searchQuery = '';
    if (typeof(props.searchQuery) !== 'undefined') {
        searchQuery = props.searchQuery;
    }

    return (
        <div className='row forum-entry mx-5 py-3'>

            <div className="col-sm-5">
                <div className="row">
                    <div className="col">
                        <h5>
                            <Link to={'/forum/' + props.forum.id}
                                  className='forum-link'>
                                <Highlighter
                                    highlightClassName='search-text-highlighted'
                                    searchWords={searchQuery.split(' ')}
                                    autoEscape={true}
                                    textToHighlight={props.forum.title}
                                />
                            </Link>
                        </h5>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <small>
                            <Highlighter
                                highlightClassName='search-text-highlighted'
                                searchWords={searchQuery.split(' ')}
                                autoEscape={true}
                                textToHighlight={props.forum.subtitle}
                            />
                        </small>
                    </div>
                </div>
            </div>
            <div className="col-sm-3 d-flex justify-content-center flex-column text-right">
                {forumStatistics}
            </div>
            <div className="col-sm-4 d-flex justify-content-center flex-column">
                <div className="row">
                    <div className="col">
                        {lastPost}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default withRouter(ForumSingle);
