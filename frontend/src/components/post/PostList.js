import React from 'react';

import * as consts from '../../constants';
import PostSingle from "./PostSingle";

class PostList extends React.Component {
    constructor(props) {
        super(props);
        let postList = this.props.posts.map((currentPost, i) =>
            <PostSingle post={currentPost} key={i}
                        lastElement={i === (this.props.posts.length-1) ? 1 : 0}
                        searchQuery={this.props.searchQuery}
                        alternateLayout={this.props.alternateLayout}
                        deletePost={this.deletePost} />
        );

        this.state = {
            deletePostUrl: consts.ANVIL_API_URL + '/posts/',
            postList: postList,
            show: 0
        };
    }

    deletePost = (id) => {
        let deletePostKey = this.state.postList.findIndex((entry) => entry.props.post.id === id);
        fetch(this.state.deletePostUrl + id, {
            method: 'DELETE',
            headers: {'Authorization' : localStorage.getItem('api_token')},
        })
            .then(
                res => {
                    if(res.ok) {
                        this.setState({postList: [...this.state.postList.slice(0, deletePostKey), ...this.state.postList.slice(deletePostKey + 1)]});
                    } else if (res.status === 401) {
                        this.props.history.push('/login');
                    } else {
                        throw Error(`${res.status} ${res.statusText}`);
                    }
                })
            .then(
                (result) => {
                    //
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    };

    render() {
        let header='';
        if (typeof(this.props.headerType) !== 'undefined') {
            if (this.props.headerType === consts.POSTS_HEADER_TYPE_NORMAL) {
                header = (
                    <h4 className="my-3 posts-header">
                        Posts
                    </h4>
                );
            } else if (this.props.headerType === consts.POSTS_HEADER_TYPE_RECENT) {
                header = (
                    <h4 className="my-3 posts-header">
                        Most Recent Posts
                    </h4>
                );
            }
        }

        if (this.state.postList.length > 0) {
            return (
                <div className='row post-list'>
                    <div className="col">
                        {header}
                        {this.state.postList}
                    </div>
                </div>
            );
        } else return null;
    }
}

export default PostList;
