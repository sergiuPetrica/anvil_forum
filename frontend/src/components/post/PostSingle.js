import React from 'react';
import { Link } from 'react-router-dom';
import moment from "moment";
import Highlighter from "react-highlight-words";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import '../../style/post.css';
import * as consts from '../../constants';

class PostSingle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            savePostUrl: consts.ANVIL_API_URL + '/posts/' + this.props.post.id,
            showEditForm: false,
            showDeleteConfirmation: false,
            message: this.props.post.message
        };
    }

    handlePostMessageChange = (event) => {
        this.setState({message: event.target.value});
        this.props.post.message = event.target.value;
    };

    togglePostEditor = () => {
        this.setState({
            showEditForm: !this.state.showEditForm
        });
    };

    togglePostDeleteConfirmation = () => {
        this.setState({
            showDeleteConfirmation: !this.state.showDeleteConfirmation
        });
    };


    saveEditedPost = () => {
        fetch(this.state.savePostUrl, {
            method: 'PUT',
            headers: {'Authorization' : localStorage.getItem('api_token'), 'Content-Type': 'application/json'},
            body: JSON.stringify({post: this.props.post})
        })
            .then(
                res => {
                    if(res.ok) {
                        return res.json();
                    } else if (res.status === 401) {
                        this.props.history.push('/login');
                    } else {
                        throw Error(`${res.status} ${res.statusText}`);
                    }
                })
            .then(
                (result) => {
                    this.togglePostEditor();
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    };

    render() {
        let searchQuery = '';
        if (typeof(this.props.searchQuery) !== 'undefined') {
            this.searchQuery = this.props.searchQuery;
        }

        let postDeleteConfirmation = (
            <div className="col d-flex flex-column justify-content-center position-absolute post-delete-overlay">
                <div className="row my-2">
                    <div className="col text-center">
                        <h5>
                            Are you sure you wish to delete this post?
                        </h5>
                    </div>
                </div>
                <div className="row my-2">
                    <div className="col text-center">
                        <button className='btn btn-primary btn-alert mx-2'
                                onClick={(id) => this.props.deletePost(this.props.post.id)}>
                            Delete
                        </button>
                        <button className='btn btn-primary mx-2'
                                onClick={() => this.togglePostDeleteConfirmation()}>
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        );

        let postEditForm = (
            <>
                <div className="col-sm-10">
                    <div className="row post-edit-form">
                        <div className="col">
                            <textarea className='form-control'
                                      rows={5}
                                      name="message"
                                      value={this.state.message}
                                      onChange={this.handlePostMessageChange} />
                        </div>
                    </div>
                    <div className="row mt-2 post-edit-buttons">
                        <div className="col d-flex justify-content-end">
                            <button className='btn btn-primary mr-2'
                                    onClick={() => this.saveEditedPost()}>
                                Save
                            </button>
                            <button className='btn btn-primary'
                                    onClick={() => this.togglePostEditor()}>
                                Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </>
        );

        let postWithControls = (
            <>
                <div className="col-sm-10">
                    <div className="row post-message">
                        <div className="col">
                            <Highlighter
                                highlightClassName='search-text-highlighted'
                                searchWords={searchQuery.split(' ')}
                                autoEscape={true}
                                textToHighlight={this.props.post.message}
                            />
                        </div>
                    </div>
                    <div className="row post-controls">
                        <div className="col-sm-3 offset-9 my-2 d-flex justify-content-between">
                            <FontAwesomeIcon className="post-control"
                                             icon={['far', 'trash-alt']}
                                             onClick={this.togglePostDeleteConfirmation} />
                            <FontAwesomeIcon className="post-control"
                                             icon={['far', 'edit']}
                                             onClick={this.togglePostEditor} />
                            <FontAwesomeIcon className="post-control"
                                             icon={['far', 'flag']} />
                            <FontAwesomeIcon className="post-control"
                                             icon={'quote-right'} />
                        </div>
                    </div>
                    {this.state.showDeleteConfirmation === true
                        ? postDeleteConfirmation
                        : null}
                </div>
            </>
        );



        // TODO: make the alternateLayout version smarter
        // It would be nice to have it group posts which belong to the same topic.
        // It should also link to the post, it currently links to the topic.
        return (
            <div className={'row post-entry mx-3 mt-2 pb-3' +
            (this.props.lastElement === 0 ? ' border-bottom' : '')}>
                <div className="col">
                    {typeof(this.props.alternateLayout) !== 'undefined'
                        ?
                        <div className="row my-3 parent-topic">
                            <div className="col">
                                <h6>
                                    In topic&nbsp;
                                    <Link to={/topic/ + this.props.post.topic.id}
                                          className={'forum-link'}>
                                        {this.props.post.topic.title}
                                    </Link>
                                </h6>
                            </div>
                        </div>
                        :
                        ''}
                    <div className="row post-header mb-3">
                        <div className="col-sm-2">
                            <div className="col user-link text-center">
                                <Link to={'/user/' + this.props.post.user.id}>
                                    <h4>
                                        {this.props.post.user.username}
                                    </h4>
                                </Link>
                            </div>
                        </div>
                        <div className="col-sm-10 post-timestamp d-flex flex-row-reverse
                            align-items-center border-bottom">
                            <h6>
                                <Link to={'/post/' + this.props.post.id}>
                                    {moment(this.props.post.created_at).fromNow().toString()}
                                </Link>
                            </h6>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-2 user-avatar text-center">
                            {this.props.post.user.avatar_path !== null
                                ? <img src={consts.ANVIL_API_URL + '/' +
                                this.props.post.user.avatar_path}
                                       alt="avatar"/>
                                : <img src={consts.USER_DEFAULT_AVATAR_PATH}
                                       alt="avatar"/>}
                        </div>
                        {this.state.showEditForm === false
                            ? postWithControls
                            : postEditForm}
                        <div className="col-sm-10 offset-2 mt-3">
                            <div className="row post-signature">
                                <div className="col mt-2">
                                    {this.props.post.user.signature}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PostSingle;
