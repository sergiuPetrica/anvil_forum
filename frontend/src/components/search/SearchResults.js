import React from 'react';

import SearchResultsForums from './SearchResultsForums';
import SearchResultsTopics from './SearchResultsTopics';
import SearchResultsPosts from './SearchResultsPosts';
import SearchResultsUsers from './SearchResultsUsers';

const SearchResults = props => {
    let searchQuery = props.match.params.search_query;

    return (
        <>
            <div className="row mb-3 search-results-header">
                <div className="col">
                    <h3>
                        Search results for '{searchQuery}':
                    </h3>
                </div>
            </div>
            <SearchResultsForums items={props.data.forums} searchQuery={searchQuery} />
            <SearchResultsTopics items={props.data.topics} searchQuery={searchQuery} />
            <SearchResultsPosts items={props.data.posts} searchQuery={searchQuery} />
            <SearchResultsUsers items={props.data.users} />
        </>
    );
};

export default SearchResults;
