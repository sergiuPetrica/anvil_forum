import React from 'react';

import ForumList from "../forum/ForumList";

const SearchResultsForums = props => {
    if (props.items.length > 0) {
        let searchQuery = props.searchQuery;
        return (
            <div className="row">
                <div className="col">
                    <ForumList forums={props.items}
                               searchQuery={searchQuery} />
                </div>
            </div>
        );
    } else return null;
};

export default SearchResultsForums;
