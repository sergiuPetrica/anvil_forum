import React from 'react';

import * as consts from '../../constants';
import PostList from '../post/PostList';

const SearchResultsPosts = props => {
    return (
        <PostList posts={props.items}
                  searchQuery={props.searchQuery}
                  alternateLayout={true}
                  headerType={consts.POSTS_HEADER_TYPE_NORMAL} />
    );
};

export default SearchResultsPosts;
