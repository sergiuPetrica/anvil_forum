import React from 'react';

import TopicList from "../topic/TopicList";

const SearchResultsTopics = props => {
    if (props.items.length > 0) {
        return <TopicList topics={props.items}
                          searchQuery={props.searchQuery} />;
    } else return null;
};

export default SearchResultsTopics;
