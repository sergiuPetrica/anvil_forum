import React from 'react';
import {withRouter} from 'react-router';

import UserList from '../user/UserList'

const SearchResultsUsers = props => {
    return <UserList users={props.items} />
};

export default withRouter(SearchResultsUsers);
