import React from 'react';

import TopicSingle from "./TopicSingle";

const TopicList = props => {
    let topicList = props.topics.map((currentTopic, i) => {
        return <TopicSingle topic={currentTopic}
                            searchQuery={props.searchQuery}
                            key={i} />
    });

    let headerText = 'Topics';
    if (typeof(props.recentHeader) !== 'undefined') {
        headerText = 'Most Recently Created Topics';
    }

    if (topicList.length > 0) {
        return (
            <>
                <h4 className="my-3 topics-header">
                    {headerText}
                </h4>
                <div className='topic-list'>
                    {topicList}
                </div>
            </>
        );
    } else return null;
};

export default TopicList;
