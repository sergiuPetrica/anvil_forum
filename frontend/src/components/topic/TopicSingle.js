import React from 'react';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Highlighter from "react-highlight-words";

const TopicSingle = (props) => {
    let topicStatistics = (
        props.topic.statistics.post_count + ' replies, ' +
        props.topic.number_of_views + ' views'
    );

    let lastPost = [];
    lastPost.push('last post by ');
    lastPost.push(
        <Link to='/user' className='forum-user'>
            {props.topic.statistics.last_post.user.username}
        </Link>
    );
    lastPost.push(' ' + moment(props.topic.statistics.last_post.created_at).fromNow().toString());

    let searchQuery = '';
    if (typeof(props.searchQuery) !== 'undefined') {
        searchQuery = props.searchQuery;
    }

    return (
        <div className='row topic-entry mx-5 py-2'>
            <div className="col-sm-3">
                <div className="row">
                    <div className="col">
                        <h5>
                            <Link to={'/topic/' + props.topic.id}
                                  className='topic-link'>
                                <Highlighter
                                    highlightClassName='search-text-highlighted'
                                    searchWords={searchQuery.split(' ')}
                                    autoEscape={true}
                                    textToHighlight={props.topic.title}
                                />
                            </Link>
                        </h5>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <small>
                            created by&nbsp;
                            <Link to={'/user/' + props.topic.user.id}
                                  className='topic-user'>
                                {props.topic.user.username}
                            </Link>&nbsp;
                            {moment(props.topic.created_at).fromNow().toString()}
                        </small>
                    </div>
                </div>
            </div>
            <div className="col-sm-3 offset-2 d-flex justify-content-center flex-column text-right">
                {topicStatistics}
            </div>
            <div className="col-sm-4 d-flex justify-content-center flex-column">
                <div className="row">
                    <div className="col">
                        {lastPost}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default withRouter(TopicSingle);
