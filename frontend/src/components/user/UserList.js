import React from 'react';

import '../../style/user.css';
import UserSingle from './UserSingle';

const UserList = props => {
    let userList = props.users.map((currentUser, i) =>
        <UserSingle user={currentUser} key={i}
                    searchQuery={props.searchQuery} />
    );

    let header = (
        <h4 className='users-header mt-5 mb-4'>
            Users
        </h4>
    );
    if (typeof(props.alternateHeader) !== 'undefined') {
        header = (
            <h4 className='users-header mt-4 mb-5'>
                User List
            </h4>
        );
    }

    if (userList.length > 0) {
        return (
            <div className="row user-list">
                <div className="col">
                    {header}
                    {userList}
                </div>
            </div>
        );
    } else return null;
};

export default UserList;
