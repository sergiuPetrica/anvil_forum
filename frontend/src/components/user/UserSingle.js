import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

import * as consts from '../../constants';

const UserSingle = props => {
    return (
        <div className="row user-entry mx-1 my-3">
            <div className="col-sm-1 user-avatar">
                {props.user.avatar_path !== null
                    ? <img src={consts.ANVIL_API_URL + '/' +
                    props.user.avatar_path}
                           alt="avatar"/>
                    : <img src={consts.USER_DEFAULT_AVATAR_PATH}
                           alt="avatar"/>}
            </div>
            <div className="col-sm-3 user-link">
                <Link to={'/user/' + props.user.id}>
                    <h4>
                        {props.user.username}
                    </h4>
                </Link>
            </div>
            <div className="col-sm-3 text-center">
                {props.user.privileges === consts.USER_ROLE_NORMAL ? 'normal user' : ''}
                {props.user.privileges === consts.USER_ROLE_MODERATOR ? 'moderator' : ''}
                {props.user.privileges === consts.USER_ROLE_ADMINISTRATOR ? 'administrator' : ''}
            </div>
            <div className="col-sm-2 text-center">
                status:&nbsp;
                {props.user.active === consts.USER_STATUS_INACTIVE ? 'inactive' : ''}
                {props.user.active === consts.USER_STATUS_ACTIVE ? 'active' : ''}
            </div>
            <div className="col-sm-3 text-right">
                created on&nbsp;
                {moment(props.user.created_at).format('MMMM Do YYYY')}
            </div>
        </div>
    );
};

export default UserSingle;
