let api_url = '';

if (process.env.NODE_ENV === 'development' || (process.env.NODE_ENV === 'test')) {
    api_url = 'http://localhost:3000';
} else if (process.env.NODE_ENV === 'production') {
    api_url = 'https://anvil-api.xicarus.com';
}

export const ANVIL_API_URL = api_url;
export const FORUM_HEADER_IMAGE_PATH = '/images/anvil_forum_logo.png';

export const USER_DEFAULT_AVATAR_PATH = '/images/default_avatar.png';
export const USER_ROLE_NORMAL = 0;
export const USER_ROLE_MODERATOR = 50;
export const USER_ROLE_ADMINISTRATOR = 100;
export const USER_STATUS_INACTIVE = 0;
export const USER_STATUS_ACTIVE = 1;

export const POSTS_HEADER_TYPE_NORMAL = 0;
export const POSTS_HEADER_TYPE_RECENT = 1;
