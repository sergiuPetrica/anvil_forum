// Library imports
import React from 'react';
import { Router, Route } from 'react-router-dom'
import createHistory from 'history/createBrowserHistory'

// Local imports
import Header from "./components/Header";
import Login from './routes/Login';
import Register from './routes/Register';
import Forum from './routes/Forum'
import Topic from './routes/Topic'
import NewTopic from './routes/NewTopic';
import NewPost from './routes/NewPost';
import NewForum from './routes/NewForum';
import Search from './routes/Search';
import User from './routes/User';
import UserListAll from "./routes/UserListAll";

const history = createHistory();

const AnvilRouter = () => (
    <Router history={history}>
        <>
            <div className="row header-section my-3">
                <Header history={history} />
            </div>
            <div className="row content-section py-4">
                <div className="col">
                    <Route path="/" exact component={Forum} />
                    <Route path="/forum/:id" render={(props) =>
                        <Forum {...props} key={window.location.pathname} />} />
                    <Route path="/topic/:id" exact component={Topic} />
                    <Route path="/topic/:id/page/:page_number" render={(props) =>
                        <Topic {...props} key={window.location.pathname} />} />
                    <Route path="/newForum" component={NewForum} />
                    <Route path="/newTopic" component={NewTopic} />
                    <Route path="/newPost" component={NewPost} />
                    <Route path="/userlist" component={UserListAll} />
                    <Route path="/user/:id" component={User} />
                    <Route path="/search/:search_query" render={(props) =>
                        <Search {...props} key={window.location.pathname} />} />
                    <Route path="/login" component={Login} />
                    <Route path="/register" component={Register} />
                </div>
            </div>
        </>
    </Router>
);

export default AnvilRouter;
