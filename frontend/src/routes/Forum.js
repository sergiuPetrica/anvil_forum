import React from 'react';

import '../style/forum.css'
import * as consts from '../constants';
import ForumList from "../components/forum/ForumList";
import TopicList from "../components/topic/TopicList";
import Breadcrumbs from "../components/Breadcrumbs";

const MODE_ROOT_FORUM = 0;
const MODE_SINGLE_FORUM = 1;

class Forum extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fetchUrl: consts.ANVIL_API_URL + '/forums/',
            mode: MODE_ROOT_FORUM,
            error: null,
            isLoaded: false,
            fetchedData: []
        };

        if (props.match.params.id != null) {
            this.state.fetchUrl += props.match.params.id;
            this.state.mode = MODE_SINGLE_FORUM;
        }
    }

    componentDidMount() {
        fetch(this.state.fetchUrl, {
            method: 'GET',
            headers: {'Authorization' : localStorage.getItem('api_token')}
        })
            .then(
                res => {
                    if(res.ok) {
                        return res.json();
                    } else if (res.status === 401) {
                        this.props.history.push('/login');
                    } else {
                        throw Error(`${res.status} ${res.statusText}`);
                    }
                })
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        fetchedData: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    handleNewTopic() {
        this.props.history.push({
            pathname: '/newTopic',
            state: {forumId: this.props.match.params.id}
        });
    }

    handleNewForum() {
        let forumIdProcessed =
            typeof(this.state.fetchedData.id) === 'undefined'
                ? null
                : this.state.fetchedData.id;
        let requiredPrivilegesProcessed =
            typeof(this.state.fetchedData.required_privileges) === 'undefined'
                ? 0
                : this.state.fetchedData.required_privileges;

        this.props.history.push({
            pathname: '/newForum',
            state: {
                forumId: forumIdProcessed,
                requiredPrivileges: requiredPrivilegesProcessed
            }
        });
    }

    render() {
        const { error, isLoaded, fetchedData } = this.state;

        if (error) {
            return <div className='message-error'>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return null;
        } else if (this.state.mode === MODE_ROOT_FORUM) {
            return (
                <>
                    <div className='row mt-2'>
                        <div className="col-sm-12">
                            <ForumList forums={fetchedData.forums} />
                        </div>
                    </div>
                    <div className="row my-4">
                        <div className="col-sm-12 d-flex align-items-end flex-column">
                            <button className='btn btn-primary'
                                    onClick={() => this.handleNewForum()}>
                                New Forum
                            </button>
                        </div>
                    </div>
                </>
            );
        } else if (this.state.mode === MODE_SINGLE_FORUM) {
            return (
                <>
                    <div className="row breadcrumbs-container">
                        <div className="col">
                            <Breadcrumbs items={fetchedData.breadcrumbs} />
                        </div>
                    </div>
                    <div className='row'>
                        <div className="col-sm-12">
                            <ForumList forums={fetchedData.forums}
                                       noCategories={true}
                                       noMessageIfEmpty={true} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12">
                            <TopicList topics={fetchedData.topics} />
                        </div>
                    </div>
                    <div className="row my-4">
                        <div className="col-sm-12 d-flex align-items-end flex-column">
                            <div className="row">
                                <div className="col-sm-6">
                                    <button className='btn btn-primary'
                                            onClick={() => this.handleNewForum()}>
                                        New Forum
                                    </button>
                                </div>
                                <div className="col-sm-6">
                                    <button className='btn btn-primary'
                                            onClick={() => this.handleNewTopic()}>
                                        New Topic
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            );
        }
        return "Error";
    }
}

export default Forum;
