import React from 'react';
import { Link } from 'react-router-dom';

import * as consts from '../constants';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null,
            error: null,
            success: null
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    handleInputChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    handleFormSubmit(event) {
        event.preventDefault();
        fetch(consts.ANVIL_API_URL + '/authenticate', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({username: this.state.username, password: this.state.password})
            })
            .then(
                res => {
                    if(res.ok) {
                        return res.json();
                    } else {
                        throw Error(`${res.status} ${res.statusText}`);
                    }
                })
            .then(
                (result) => {
                    localStorage.setItem('api_token', result.auth_token);
                    this.props.history.push('/');
                },
                (error) => {
                    this.setState({
                        error: error.message
                    });
                }
            );
    }

    render() {
        return (
            <form onSubmit={this.handleFormSubmit}>
                <div className="row my-4">
                    <div className="col-sm-6 offset-sm-3">
                        <h4>
                            Login
                        </h4>
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-sm-6 offset-sm-3">
                        <h6>
                            Username&nbsp;
                        </h6>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6 offset-sm-3">
                        <input className='form-control'
                               type="text"
                               name="username"
                               onChange={this.handleInputChange} />
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-sm-6 offset-sm-3">
                        <h6>
                            Password&nbsp;
                        </h6>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6 offset-sm-3">
                        <input className='form-control'
                               type="password"
                               name="password"
                               onChange={this.handleInputChange} />
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-sm-3 offset-sm-3">
                        <input className='btn btn-primary'
                               type="submit"
                               value="Submit" />
                    </div>
                    <div className="col-sm-3 d-flex align-items-center
                            justify-content-end">
                        <Link to={'/register'}>
                            No account? Create one!
                        </Link>
                    </div>
                </div>
            </form>
        );
    }
}

export default Login;