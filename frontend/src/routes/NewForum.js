import React from 'react';

import * as consts from '../constants';

class NewForum extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: null,
            subtitle: null,
            requiredPrivileges: null,
            error: null,
            requiresLogin: false,
            success: null
        };

        if (typeof(this.props.location.state) !== 'undefined') {
            this.state.requiredPrivileges = this.props.location.state.requiredPrivileges;
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    handleInputChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    handleFormSubmit(event) {
        event.preventDefault();
        fetch(consts.ANVIL_API_URL + '/forums', {
            method: 'POST',
            headers: {'Authorization' : localStorage.getItem('api_token'), 'Content-Type': 'application/json'},
            body: JSON.stringify({
                title: this.state.title,
                subtitle: this.state.subtitle,
                parent_id: this.props.location.state.forumId,
                required_privileges: this.state.requiredPrivileges
            })
        })
            .then(
                res => {
                    if(res.ok) {
                        return res.json();
                    } else if (res.status === 401) {
                        this.props.history.push('login');
                    } else {
                        throw Error(`${res.status} ${res.statusText}`);
                    }
                })
            .then(
                (result) => {
                    let redirectUrl = '/forum/' + result.id;
                    this.props.history.push(redirectUrl);
                },
                (error) => {
                    this.setState({
                        error: error.message
                    });
                }
            );
    }

    render() {
        return (
            <form onSubmit={this.handleFormSubmit}>
                <div className="row my-4">
                    <div className="col-sm-6 offset-sm-3">
                        <h4>
                            New Forum
                        </h4>
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-sm-6 offset-sm-3">
                        <h6>
                            Forum Title
                        </h6>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6 offset-sm-3">
                        <input className='form-control'
                               type="text"
                               name="title"
                               onChange={this.handleInputChange}/>
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-sm-6 offset-sm-3">
                        <h6>
                            Forum Subtitle
                        </h6>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6 offset-sm-3">
                        <input className='form-control'
                               type="text"
                               name="subtitle"
                               onChange={this.handleInputChange}/>
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-sm-6 offset-sm-3">
                        <h6>
                            Required Privileges
                        </h6>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6 offset-sm-3">
                        <input className='form-control'
                               type="text"
                               name="requiredPrivileges"
                               onChange={this.handleInputChange}
                               value={this.state.requiredPrivileges}/>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-sm-6 offset-sm-3">
                        <input className='btn btn-primary'
                               type="submit"
                               value="Submit"/>
                    </div>
                </div>
            </form>
        );
    }
}

export default NewForum;
