import React from 'react';

import * as consts from '../constants';

class NewTopic extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: null,
            content: null,
            error: null,
            success: null
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    handleInputChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    handleFormSubmit(event) {
        event.preventDefault();
        fetch(consts.ANVIL_API_URL + '/topics', {
            method: 'POST',
            headers: {'Authorization' : localStorage.getItem('api_token'), 'Content-Type': 'application/json'},
            body: JSON.stringify({
                title: this.state.title,
                forum_id: this.props.location.state.forumId,
                content: this.state.content
            })
        })
            .then(
                res => {
                    if(res.ok) {
                        return res.json();
                    } else if (res.status === 401) {
                        this.props.history.push('/login');
                    } else {
                        throw Error(`${res.status} ${res.statusText}`);
                    }
                })
            .then(
                (result) => {
                    let redirectUrl = '/topic/' + result.topic_id;
                    this.props.history.push(redirectUrl);
                },
                (error) => {
                    this.setState({
                        error: error.message
                    });
                }
            );
    }

    render() {
        return (
            <form onSubmit={this.handleFormSubmit}>
                <div className="row my-4">
                    <div className="col-sm-6 offset-sm-3">
                        <h4>
                            New Topic
                        </h4>
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-sm-6 offset-sm-3">
                        <h6>
                            Topic Title
                        </h6>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6 offset-sm-3">
                        <input className='form-control'
                               type="text"
                               name="title"
                               onChange={this.handleInputChange} />
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-sm-6 offset-sm-3">
                        <h6>
                            Content
                        </h6>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6 offset-sm-3">
                        <textarea className='form-control'
                                  rows={5}
                                  name="content"
                                  onChange={this.handleInputChange} />
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-sm-6 offset-sm-3">
                        <input className='btn btn-primary'
                               type="submit"
                               value="Submit" />
                    </div>
                </div>
            </form>
        );
    }
}

export default NewTopic;
