import React from 'react';
import { Link } from 'react-router-dom';

import * as consts from "../constants";

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            email: null,
            password: null,
            error: null,
            success: null
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    handleInputChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    handleFormSubmit(event) {
        event.preventDefault();
        fetch(consts.ANVIL_API_URL + '/register', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({username: this.state.username,
                                  email: this.state.email,
                                  password: this.state.password})
        })
            .then(
                res => {
                    if(res.ok) {
                        return res.json();
                    } else {
                        throw Error(`${res.status} ${res.statusText}`);
                    }
                })
            .then(
                (result) => {
                    this.setState({ success: 1 });
                },
                (error) => {
                    this.setState({
                        error: error.message
                    });
                }
            );
    }

    render() {
        const { error } = this.state;

        if (this.state.error !== null) {
            return (
                <div className="registration-error">
                    An error has occured: {error}
                </div>
            );
        } else if (this.state.success === null) {
            return (
                <div className="registration-form">
                    <form onSubmit={this.handleFormSubmit}>
                        <div className="row my-4">
                            <div className="col-sm-6 offset-sm-3">
                                <h4>
                                    User Registration
                                </h4>
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-sm-6 offset-sm-3">
                                <h6>
                                    Username&nbsp;
                                </h6>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6 offset-sm-3">
                                <input className='form-control'
                                       type="text"
                                       name="username"
                                       onChange={this.handleInputChange} />
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-sm-6 offset-sm-3">
                                <h6>
                                    Email&nbsp;
                                </h6>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6 offset-sm-3">
                                <input className='form-control'
                                       type="text"
                                       name="email"
                                       onChange={this.handleInputChange} />
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-sm-6 offset-sm-3">
                                <h6>
                                    Password&nbsp;
                                </h6>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6 offset-sm-3">
                                <input className='form-control'
                                       type="password"
                                       name="password"
                                       onChange={this.handleInputChange} />
                            </div>
                        </div>

                        <div className="row mt-4">
                            <div className="col-sm-6 offset-sm-3">
                                <input className='btn btn-primary'
                                       type="submit"
                                       value="Submit" />
                            </div>
                        </div>
                    </form>
                </div>
            );
        } else if (this.state.success === 1) {
            return (
                <div className="registration-successful">
                    <div className="row my-4">
                        <div className="col-sm-6 offset-sm-3">
                            <h4>
                                Thank you for registering. You may now&nbsp;
                                <Link to='/login'>log in</Link> with your credentials.
                            </h4>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="registration-error">
                    Unknown error
                </div>
            );
        }
    }
}

export default Register;
