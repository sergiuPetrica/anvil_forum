import React from 'react';

import '../style/search.css';
import * as consts from '../constants';
import SearchResults from '../components/search/SearchResults';

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchQuery: this.props.match.params.search_query,
            fetchedData: null,
            error: null,
            success: null
        };

        // this.handleInputChange = this.handleInputChange.bind(this);
        // this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    componentDidMount() {
        fetch(consts.ANVIL_API_URL + '/search/' + this.state.searchQuery, {
            method: 'GET',
            headers: {'Authorization' : localStorage.getItem('api_token')},
        })
            .then(
                res => {
                    if(res.ok) {
                        return res.json();
                    } else if (res.status === 401) {
                        this.props.history.push('/login');
                    } else {
                        throw Error(`${res.status} ${res.statusText}`);
                    }
                })
            .then(
                (result) => {
                    this.setState({ success: 1,  fetchedData: result });
                },
                (error) => {
                    this.setState({
                        error: error.message
                    });
                }
            );
    }

    // handleInputChange(event) {
    //     this.setState({[event.target.name]: event.target.value});
    // }

    render() {
        const { fetchedData, error } = this.state;

        if (this.state.error !== null) {
            return (
                <div className="general-error">
                    An error has occurred: {error}
                </div>
            );
        } else if (this.state.success === null) {
            return null;
        } else if (this.state.success === 1) {
            return <SearchResults data={fetchedData} match={this.props.match} />
        } else {
            return (
                <div className="general-error">
                    An unknown error has occurred.
                </div>
            );
        }
    }
}

export default Search;
