import React from 'react';
import { Link } from 'react-router-dom';
import moment from "moment";

import '../style/topic.css';
import * as consts from '../constants';
import PostList from '../components/post/PostList';
import Breadcrumbs from "../components/Breadcrumbs";
import Pagination from '../components/Pagination';

class Topic extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fetchUrl: consts.ANVIL_API_URL + '/topics/' + props.match.params.id,
            error: null,
            isLoaded: false,
            fetchedData: [],
        };
        if (typeof(props.match.params.page_number) !== 'undefined') {
            this.state.fetchUrl += '/page/' + props.match.params.page_number;
        }
    }

    componentDidMount() {
        fetch(this.state.fetchUrl, {
            method: 'GET',
            headers: {'Authorization' : localStorage.getItem('api_token')}
        })
            .then(
                res => {
                    if(res.ok) {
                        return res.json();
                    } else if (res.status === 401) {
                        this.props.history.push('/login');
                    } else {
                        throw Error(`${res.status} ${res.statusText}`);
                    }
                })
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        fetchedData: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    handleNewPost() {
        this.props.history.push({
            pathname: '/newPost',
            state: {topicId: this.props.match.params.id}
        });
    }

    render() {
        const { error, isLoaded, fetchedData } = this.state;

        if (error) {
            return <div className='message-error'>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return null;
        } else {
            return (
                <>
                    <div className="row mb-5 topic-info">
                        <div className="col ml-3">
                            <div className="row">
                                <div className="col">
                                    <h3>
                                        <Link className='topic-title'
                                              to={'/topic/' + fetchedData.id}>
                                            {fetchedData.title}
                                        </Link>
                                    </h3>
                                </div>
                            </div>
                            <div className="row ml-3 mb-2">
                                <div className="col">
                                    <Breadcrumbs items={fetchedData.breadcrumbs} />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    Discussion started by&nbsp;
                                    <Link to={'/user/' + fetchedData.user.id}>
                                        {fetchedData.user.username}
                                    </Link>&nbsp;
                                    {moment(fetchedData.created_at).fromNow().toString()}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <PostList posts={fetchedData.posts} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6 d-flex align-items-center flex-column">
                            <h6>
                                <Pagination topic_id={this.props.match.params.id}
                                            current_page_number={fetchedData.current_page_number}
                                            number_of_pages={fetchedData.number_of_pages} />
                            </h6>
                        </div>
                        <div className="col-sm-6 d-flex align-items-end flex-column">
                            <button className='btn btn-primary'
                                    onClick={() => this.handleNewPost()}>
                                Reply
                            </button>
                        </div>
                    </div>
                </>
            )
        }
    }
}

export default Topic;
