import React from 'react';
import moment from 'moment';

import '../style/user.css';
import * as consts from '../constants';
import TopicList from "../components/topic/TopicList";
import PostList from "../components/post/PostList";

class User extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fetchUrl: consts.ANVIL_API_URL + '/users/' + props.match.params.id,
            error: null,
            isLoaded: false,
            fetchedData: [],
        };
    }

    componentDidMount() {
        fetch(this.state.fetchUrl, {
            method: 'GET',
            headers: {'Authorization' : localStorage.getItem('api_token')}
        })
            .then(
                res => {
                    if(res.ok) {
                        return res.json();
                    } else if (res.status === 401) {
                        this.props.history.push('/login');
                    } else {
                        throw Error(`${res.status} ${res.statusText}`);
                    }
                })
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        fetchedData: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        const { error, isLoaded, fetchedData } = this.state;

        if (typeof(fetchedData.birthday) === 'undefined' ||
                fetchedData.birthday === null) {
            fetchedData.birthday = 'none';
        } else {
            fetchedData.birthday = (
                moment(fetchedData.birthday).format('Do of MMMM YYYY') +
                ' (' +
                moment().diff(fetchedData.birthday, 'years') +
                ' years old' +
                ')'
            );
        }

        if (typeof(fetchedData.location) === 'undefined' ||
                fetchedData.location === null) {
            fetchedData.location = 'none';
        }

        if (typeof(fetchedData.occupation) === 'undefined' ||
                fetchedData.occupation === null) {
            fetchedData.occupation = 'none';
        }

        if (typeof(fetchedData.mood) === 'undefined' ||
                fetchedData.mood === null) {
            fetchedData.mood = 'none';
        }

        if (error) {
            return <div className='message-error'>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return null;
        } else {
            return (
                <>
                    <div className="row">
                        <div className="col-sm-3 left-side">
                            <div className="row user-username mt-3">
                                <div className="col text-center tertiary-color">
                                    <h3>
                                        {fetchedData.username}
                                    </h3>
                                </div>
                            </div>
                            <div className="row user-role mb-3">
                                <div className="col text-center">
                                    {fetchedData.privileges === consts.USER_ROLE_NORMAL
                                        ? 'Normal User' : ''}
                                    {fetchedData.privileges === consts.USER_ROLE_MODERATOR
                                        ? 'Moderator' : ''}
                                    {fetchedData.privileges === consts.USER_ROLE_ADMINISTRATOR
                                        ? 'Administrator' : ''}
                                </div>
                            </div>
                            <div className="row user-avatar mb-5">
                                <div className="col text-center">
                                    {fetchedData.avatar_path !== null
                                        ? <img src={consts.ANVIL_API_URL + '/' +
                                        fetchedData.avatar_path}
                                               alt="avatar"/>
                                        : <img src={consts.USER_DEFAULT_AVATAR_PATH}
                                               alt="avatar"/>}
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-5 .center-side mt-5">
                            <div className="row user-details">
                                <div className="col">
                                    <div className="row user-birthday mb-3">
                                        <div className="col">
                                            <h5>
                                                Birthday
                                            </h5>
                                        </div>
                                        <div className="w-100"></div>
                                        <div className="col ml-3">
                                            {fetchedData.birthday}
                                        </div>
                                    </div>
                                    <div className="row user-location mb-3">
                                        <div className="col">
                                            <h5>
                                                Location
                                            </h5>
                                        </div>
                                        <div className="w-100"></div>
                                        <div className="col ml-3">
                                            {fetchedData.location}
                                        </div>
                                    </div>
                                    <div className="row user-occupation mb-3">
                                        <div className="col">
                                            <h5>
                                                Occupation
                                            </h5>
                                        </div>
                                        <div className="w-100"></div>
                                        <div className="col ml-3">
                                            {fetchedData.occupation}
                                        </div>
                                    </div>
                                    <div className="row user-mood mb-3">
                                        <div className="col">
                                            <h5>
                                                Mood
                                            </h5>
                                        </div>
                                        <div className="w-100"></div>
                                        <div className="col ml-3">
                                            {fetchedData.mood}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4 right-side mt-5">
                            <div className="row user-number-of-posts mb-3">
                                <div className="col">
                                    <h5>
                                        Number of posts
                                    </h5>
                                </div>
                                <div className="w-100"></div>
                                <div className="col ml-3">
                                    {fetchedData.statistics.number_of_posts}
                                </div>
                            </div>
                            <div className="row user-number-of-topics mb-3">
                                <div className="col">
                                    <h5>
                                        Number of created topics
                                    </h5>
                                </div>
                                <div className="w-100"></div>
                                <div className="col ml-3">
                                    {fetchedData.statistics.number_of_topics}
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-12 bottom-side">
                            <div className="row recent-posts mt-5">
                                <div className="col">
                                    <PostList posts={fetchedData.statistics.recent_posts}
                                              alternateLayout={true}
                                              headerType={consts.POSTS_HEADER_TYPE_RECENT} />
                                </div>
                            </div>
                            <div className="row recent-topics mt-5">
                                <div className="col">
                                    <TopicList topics={fetchedData.statistics.recent_topics}
                                               recentHeader={true} />
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            )
        }
    }
}

export default User;
