import React from 'react';

import * as consts from '../constants';
import UserList from "../components/user/UserList";

class UserListAll extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fetchUrl: consts.ANVIL_API_URL + '/users/',
            error: null,
            isLoaded: false,
            fetchedData: [],
        };
    }

    componentDidMount() {
        fetch(this.state.fetchUrl, {
            method: 'GET',
            headers: {'Authorization' : localStorage.getItem('api_token')}
        })
            .then(
                res => {
                    if(res.ok) {
                        return res.json();
                    } else if (res.status === 401) {
                        this.props.history.push('/login');
                    } else {
                        throw Error(`${res.status} ${res.statusText}`);
                    }
                })
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        fetchedData: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        const { error, isLoaded, fetchedData } = this.state;

        if (error) {
            return <div className='message-error'>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return null;
        } else {
            return <UserList users={fetchedData}
                             alternateHeader={true} />
        }
    }
}

export default UserListAll;
